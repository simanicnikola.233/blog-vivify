<?php include 'db.php'; ?>

<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Vivify Blog</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="styles/styles.css" rel="stylesheet">
    <link href="styles/blog.css" rel="stylesheet">
</head>

<form class="" action="create-post.php" method="post">
    <label for="title">Title</label>
    <input type="text" name="Title" id="">
    <br><br>
    <label for="author">Author</label>
    <input type="text" name="Author" id="">
    <br><br>
    <label for="body">Body</label>
    <input type="text" name="Body" id="">
    <br><br>
    <label for="created_at">Created At</label>
    <input type="date" name="Created At">
    <br><br>
</form>