<?php
include 'db.php';
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">
    <title>Vivify Blog</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="styles/styles.css" rel="stylesheet">
    <link href="styles/blog.css" rel="stylesheet">
</head>

<body>
    <?php include 'header.php' ?>
    <?php
    if (isset($_GET['post_id'])) {
        $sql = "SELECT p.id, p.title, p.body, p.author, p.created_at FROM posts AS p WHERE p.id = {$_GET['post_id']}";
        $post = getData($connection, $sql);

        $sql2 = "SELECT * FROM comments AS c LEFT JOIN posts AS p ON c.post_id = p.id WHERE p.id = {$_GET['post_id']};";
        $comm = getAllData($connection, $sql2);

    ?>
        <main role="main" class="container">
            <div class="row">
                <div class="col-sm-8 blog-main">
                    <div class="blog-post">
                        <a href="single-post.php">
                            <h2 class="blog-post-title"><?php echo $post['title'] ?></h2>
                        </a>
                        <p class="blog-post-meta"><?php echo $post['created_at'] ?><a href="#"> <?php echo $post['author'] ?></a></p>
                        <p><?php echo $post['body'] ?></p>
                    </div><!-- /.blog-post -->
                    <div class="comments">
                        <h4>Comments</h4>
                        <ul class="class-for-ul">
                            <?php foreach ($comm as $comment) { ?>
                                <li>
                                    <p>
                                        <?php echo $comment['username'] ?>
                                    </p>
                                    <p>
                                        <?php echo $comment['text'] ?>
                                    </p>
                                </li>
                                <hr>
                            <?php } ?>
                        </ul>
                    </div>
                <?php
            } else {
                echo ("nema komentara");
            }
                ?>
                <nav class="blog-pagination">
                    <a class="btn btn-outline-primary" href="#">Older</a>
                    <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
                </nav>
                </div><!-- /.blog-main -->
                <?php include 'footer.php' ?>
</body>

</html>